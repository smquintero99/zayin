import React from 'react'; import {View, FlatList} from 'react-native';
import {ListItem, Button} from 'react-native-elements'; import {Goals} from '../data/goals'; 
import firebase from '../config/firebase'; export default 
class Main extends React.Component {constructor(props){super(props);this.state={goals:Goals}
  this.ref = firebase.firestore().collection('Goals')}
 saveBoard=()=>{this.ref.add({Portuguese:this.state.goals[0].checked, Scala:this.state.goals[1].checked,
   'Reinforcement Learning':this.state.goals[2].checked, 'Neuro-Linguistic Programming':this.state.goals[3].checked,
   Excercise:this.state.goals[4].checked, 'Business Project':this.state.goals[5].checked,
   'Personal Project':this.state.goals[6].checked, Family:this.state.goals[7].checked,
   Date:this.state.goals[8].checked, Sex:this.state.goals[9].checked, Spirit:this.state.goals[10].checked})
 .then((docRef) => {console.log('Success from Menu')})
 .catch((error) => {console.error("Error adding document: ", error)})}
 changeName=(id)=>{let newArray = this.state.goals.slice(); newArray[id]={
  ...this.state.goals[id],checked:!this.state.goals[id].checked,};this.setState({goals:newArray})}
 render(){renderMenuItem = ({item,index}) =>{
  return(<ListItem key={index} title={item.name} hideChevron switchButton onPress={
   ()=>this.changeName(index)} switched={item.checked} onSwitch={()=>this.changeName(index)}/>)}
  return(<View><FlatList data={this.state.goals} renderItem={renderMenuItem} 
    keyExtractor={item=>item.id.toString()}/><View style={{marginTop: 16}}>
   <Button title='Send' backgroundColor="#5cff5c" onPress={() => this.saveBoard()} /></View></View> )}}
